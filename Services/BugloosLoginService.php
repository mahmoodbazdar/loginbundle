<?php

namespace Bugloos\LoginBundle\Services;

use AppBundle\Entity\User;
use Bugloos\LoginBundle\Form\ForgotPasswordType;
use Bugloos\LoginBundle\Form\LoginType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

class BugloosLoginService
{

    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param Request $request
     *
     * @param array $options
     *
     * @return array|\Symfony\Component\Form\FormInterface
     *
     */
    public function login($request, $options = [])
    {
        $formData = [
            "email" => $request->getUser(),
            "password" => $request->getPassword(),
        ];
        $user = new $options['data_class']();
        $loginForm = $this->container->get('form.factory')->create(LoginType::class, $user, $options);
        $loginForm->submit($formData);
        if (!$loginForm->isValid()) {
            return $loginForm;
        }
        $user = $this->container->get('doctrine')
            ->getRepository($options['data_class'])
            ->findOneBy(['email' => $request->getUser()]);
        if (!$user) {
            throw new NotFoundHttpException(sprintf('Page Not Found'));
        }
        $isValid = $this->container->get('security.password_encoder')
            ->isPasswordValid($user, $request->getPassword());
        if (!$isValid) {
            throw new BadCredentialsException();
        }
        $token = $this->container->get('lexik_jwt_authentication.encoder')
            ->encode([
                'email' => $user->getUsername(),
            ]);

        return ['token' => $token, 'user' => $user];

    }

    public function resetPassword(Request $request, $entity)
    {
        $key = $request->query->get("token");
        $newPassword = $request->request->get("password");
        if (!$key || !$newPassword) {
            return false;
        }
        /** @var User $user */
        $user = $this->container->get('doctrine')->getRepository($entity)->findOneBy(["token" => $key]);
        if (!$user) {
            return false;
        }
        $password = $this->container->get('security.password_encoder')->encodePassword($user, $newPassword);;
        $user->setPassword($password);
        $user->setLastRequest(new \DateTime('now'));
        $this->container->get('doctrine')->getManager()->persist($user);
        $this->container->get('doctrine')->getManager()->flush();

        return $user;
    }

    public function forgotPassword(Request $request, $options = [])
    {
        $formData = [
            "email" => $request->request->get("email", null),
        ];
        $user = new $options['data_class']();
        $forgotPassword = $this->container->get('form.factory')->create(ForgotPasswordType::class, $user, $options);
        $forgotPassword->submit($formData);
        if (!$forgotPassword->isValid()) {
            return $forgotPassword;
        }
        /**
         * @var User $user
         */
        $user = $forgotPassword->getData();
        $user = $this->container->get('doctrine')->getRepository($options['data_class'])
            ->findOneBy(["email" => $user->getEmail()]);
        $pwdResetHash = hash('sha256', time() . $this->container->getParameter('secret'));
        $user->setToken($pwdResetHash);
        $this->container->get('doctrine')->getManager()->persist($user);
        $this->container->get('doctrine')->getManager()->flush();

        return ['hash' => $pwdResetHash, 'user' => $user];
    }


}