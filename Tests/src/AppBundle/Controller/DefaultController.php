<?php

    namespace AppBundle\Controller;

    use AppBundle\Entity\User;
    use Bugloos\LoginBundle\Exceptions\ApiProblem;
    use Bugloos\LoginBundle\Exceptions\ApiProblemException;
    use Doctrine\Common\Persistence\ObjectManager;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use Symfony\Component\Form\FormInterface;
    use Symfony\Component\HttpFoundation\JsonResponse;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\Security\Core\Exception\BadCredentialsException;

    class DefaultController extends Controller {

        public function indexAction() {
            $user = new User();
            $user->setEmail('knightvoly@gmail.com');
            $user->setPassword($this->get('security.password_encoder')->encodePassword($user, '12321'));
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
        }

        /**
         * @param Request $request
         *
         * @return \Symfony\Component\HttpFoundation\JsonResponse
         */
        public function loginAction(Request $request) {
            $token = $this->get('bugloos.login')->login($request, ['data_class' => 'AppBundle\Entity\User']);
            if ($token instanceof FormInterface) {
                throw new BadCredentialsException();
            }

            return new JsonResponse(['token' => $token]);
        }

        /**
         *
         * @param Request $request
         *
         * @return JsonResponse
         */
        public function resetPasswordAction(Request $request) {
            $user = $this->get('bugloos.login')->resetPassword($request, 'AppBundle\Entity\User');
            if ($user) {
                return new JsonResponse(['user' => $user->getId()]);
            }
            $apiProblem = new ApiProblem(400, 'kachal');
            $apiProblem->set('errors', ['']);
            throw new ApiProblemException($apiProblem);
        }


        /**
         *
         * @param \Symfony\Component\HttpFoundation\Request $request
         *
         * @return \Symfony\Component\HttpFoundation\Response
         */
        public function forgotPasswordAction(Request $request) {
            $result = $this->get('bugloos.login')->forgotPassword($request, ['data_class' => 'AppBundle\Entity\User']);
            if ($result instanceof FormInterface) {
                throw new BadCredentialsException();
            }

            return new JsonResponse(['token' => true]);
        }

    }
