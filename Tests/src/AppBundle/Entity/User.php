<?php

    namespace AppBundle\Entity;

    use Bugloos\LoginBundle\Entity\BugloosUserInterface;
    use Symfony\Component\Security\Core\User\UserInterface;
    use Doctrine\ORM\Mapping as ORM;

    /**
     * @ORM\Entity
     * @ORM\HasLifecycleCallbacks()
     * @ORM\Table(name="user")
     */
    class User implements BugloosUserInterface, UserInterface
    {
        /**
         * @ORM\Id
         * @ORM\GeneratedValue(strategy="AUTO")
         * @ORM\Column(type="integer")
         */
        private $id;
        /**
         * @ORM\Column(type="string")
         */
        private $email;
        /**
         * @ORM\Column(type="string")
         */
        private $password;
        /**
         * @ORM\Column(type="string",nullable=true)
         */
        private $token;


        /**
         * @var \DateTime
         *
         * @ORM\Column(name="createdAt", type="datetime")
         */
        private $createdAt;
        /**
         * @var \DateTime
         *
         * @ORM\Column(name="last_request", type="datetime",nullable=true)
         */
        private $lastRequest;

        /**
         * @var \DateTime
         *
         * @ORM\Column(name="updatedAt", type="datetime")
         */
        private $updatedAt;

        /**
         * @return mixed
         */
        public function getEmail()
        {
            return $this->email;
        }

        /**
         * @param mixed $email
         */
        public function setEmail($email)
        {
            $this->email = $email;
        }

        /**
         * @return mixed
         */
        public function getPassword()
        {
            return $this->password;
        }

        /**
         * @param mixed $password
         */
        public function setPassword($password)
        {
            $this->password = $password;
        }

        /**
         * @return mixed
         */
        public function getToken()
        {
            return $this->token;
        }

        /**
         * @param mixed $token
         */
        public function setToken($token)
        {
            $this->token = $token;
        }

        /**
         * @return mixed
         */
        public function getId()
        {
            return $this->id;
        }


        public function getRoles()
        {
            return ['ROLE_ADMIN'];
        }

        public function getSalt()
        {
            return;
        }

        public function getUsername()
        {
            return $this->email;
        }

        public function eraseCredentials()
        {
            // TODO: Implement eraseCredentials() method.
        }

        public function getLastRequest()
        {
            return $this->lastRequest;
        }

        public function setLastRequest($lastRequest)
        {
            $this->lastRequest = $lastRequest;
        }

        /**
         * Set createdAt
         *
         * @param \DateTime $createdAt
         *
         * @return User
         */
        public function setCreatedAt($createdAt)
        {
            $this->createdAt = $createdAt;
            return $this;
        }

        /**
         * Get createdAt
         *
         * @return \DateTime
         */
        public function getCreatedAt()
        {
            return $this->createdAt;
        }

        /**
         * Set updatedAt
         *
         * @param \DateTime $updatedAt
         *
         * @return User
         */
        public function setUpdatedAt($updatedAt)
        {
            $this->updatedAt = $updatedAt;
            return $this;
        }

        /**
         * Get updatedAt
         *
         * @return \DateTime
         */
        public function getUpdatedAt()
        {
            return $this->updatedAt;
        }

        /**
         * @ORM\PrePersist()
         * @ORM\PreUpdate()
         */
        public function updatedTimestamps()
        {
            $this->setUpdatedAt(new \DateTime('now'));
            if ($this->getCreatedAt() == null) {
                $this->setCreatedAt(new \DateTime('now'));
            }
        }
    }