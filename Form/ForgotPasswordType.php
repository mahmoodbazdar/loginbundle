<?php

    namespace Bugloos\LoginBundle\Form;

    use AppBundle\Entity\User;
    use Doctrine\ORM\EntityManager;
    use Symfony\Component\DependencyInjection\ContainerInterface;
    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\Extension\Core\Type\EmailType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\Form\FormError;
    use Symfony\Component\Form\FormEvent;
    use Symfony\Component\Form\FormEvents;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\HttpFoundation\RequestStack;
    use Symfony\Component\OptionsResolver\OptionsResolver;
    use Symfony\Component\Translation\Translator;

    class ForgotPasswordType extends AbstractType {

        /**
         * @var EntityManager
         */
        private $manager;
        /**
         * @var Request
         */
        private $request;
        /**
         * @var
         */
        private $ban_minutes;

        /**
         * @var
         */
        private $ban_counts;

        /**
         * @var $translator
         */
        private $translator;
        /**
         * @var ContainerInterface
         */
        private $container;

        use BanUsersTrait;

        /**
         * ResetPasswordType constructor.
         *
         * @param EntityManager $manager
         * @param ContainerInterface $container
         * @param RequestStack $request
         * @param Translator $translator
         */
        public function __construct(EntityManager $manager, ContainerInterface $container, RequestStack $request, Translator $translator) {
            $this->manager     = $manager;
            $this->container   = $container;
            $this->request     = $request->getCurrentRequest();
            $this->ban_minutes = $this->container->getParameter('ban_minutes');
            $this->ban_counts  = $this->container->getParameter('ban_max_count');
            $this->translator  = $translator;
        }

        public function buildForm(FormBuilderInterface $builder, array $options) {
            $builder->add("email", EmailType::class);
            $builder->addEventListener(
                FormEvents::POST_SUBMIT,
                function (FormEvent $event) use ($options) {
                    if ( ! $event->getForm()->isValid()) {
                        return;
                    }
                    /**
                     * @var User $user
                     */
                    $user = $event->getForm()->getData();
                    $ban  = $this->checkBanUser($event);
                    $loadedUser = $this->manager->getRepository($options['data_class'])->findOneBy(["email" => $user->getEmail()]);
                    if ( ! $loadedUser) {
                        $this->manageBanUsers($ban);
                        $event->getForm()->addError(new FormError($this->translator->trans("forgot password.email dose not exist", [], "validators")));

                        return;
                    }

                    if ($event->getForm()->getErrors()->count() > 0) {
                        return;
                    }

                    if ( ! $this->canRequestAgain($loadedUser)) {
                        return;
                    }

                    $event->setData($loadedUser);
                });
        }

        public function configureOptions(OptionsResolver $resolver) {
            $resolver->setDefaults(
                [
                    'allow_extra_fields' => true,
                    "csrf_protection"    => false,
                ]);
        }

        public function getName() {
            return 'app_bundle_reset_password_type';
        }
    }