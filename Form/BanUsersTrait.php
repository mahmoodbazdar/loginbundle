<?php

    namespace Bugloos\LoginBundle\Form;

    use AppBundle\Entity\User;
    use Bugloos\LoginBundle\Entity\BlockIp;
    use DateInterval;
    use Doctrine\ORM\EntityManager;
    use Symfony\Component\Form\FormError;
    use Symfony\Component\Form\FormEvent;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\Translation\Translator;

    trait BanUsersTrait {

        /**
         * @var Translator $translator
         */
        private $translator;
        /**
         * @var EntityManager
         */
        private $manager;
        /**
         * @var Request
         */
        private $request;
        /**
         * @var integer
         */
        private $ban_minutes;
        /**
         * @var integer
         */
        private $ban_counts;


        private function checkBanUser(FormEvent $event) {
            $ban = $this->manager->getRepository('LoginBundle:BlockIp')->findOneBy([
                'ip'   => $this->request->getClientIp(),
                'type' => self::class == LoginType::class ? BlockIp::LOGIN_TYPE : BlockIp::FORGOT_PASSWORD_TYPE,
            ]);
            if ($ban && $ban->getCounter() > $this->ban_counts) {
                $date = $ban->getCreatedAt()->add(new DateInterval('PT'.$this->ban_minutes.'M'));
                if ($date > $ban->getUpdatedAt()) {
                    $event->getForm()->addError(
                        new FormError($this->translator->trans("login.your ip is banned",
                            [
                                "{{update}}" => $date->diff($ban->getUpdatedAt())->i,
                            ],
                            "validators"))
                    );
                    $ban->setCounter($ban->getCounter() + 1);
                    $this->manager->persist($ban);
                    $this->manager->flush();
                } else {
                    $ban->setCounter(0);
                    $ban->setCreatedAt(new \DateTime());
                    $this->manager->persist($ban);
                    $this->manager->flush();
                }
            }

            return $ban;
        }

        public function canRequestAgain($user) {
            if(is_null($user->getLastRequest())){
                return true;
            }
            $date = $user->getLastRequest()->add(new DateInterval('PT'.$this->ban_minutes.'M'));
            if ($date > new \DateTime('now')) {
                return false;
            }

            return true;
        }

        private function manageBanUsers(BlockIp $ban = null) {
            if ($ban) {
                $ban->setCounter($ban->getCounter() + 1);
                $this->manager->persist($ban);
            } else {
                $newBlockIP = new BlockIp();
                $newBlockIP->setIp($this->request->getClientIp());
                $newBlockIP->setCounter(1);
                $newBlockIP->setType(self::class == LoginType::class ? BlockIp::LOGIN_TYPE : BlockIp::FORGOT_PASSWORD_TYPE);
                $this->manager->persist($newBlockIP);
            }
            $this->manager->flush();
        }
    }