<?php

    namespace Bugloos\LoginBundle\Form;

    use Doctrine\ORM\EntityManager;
    use Symfony\Component\DependencyInjection\ContainerInterface;
    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\Form\FormError;
    use Symfony\Component\Form\FormEvent;
    use Symfony\Component\Form\FormEvents;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\HttpFoundation\RequestStack;
    use Symfony\Component\OptionsResolver\OptionsResolver;
    use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
    use Symfony\Component\Translation\Translator;

    class LoginType extends AbstractType {

        /**
         * @var EntityManager
         */
        private $manager;

        private $container;
        /**
         * @var UserPasswordEncoder
         */
        private $encoder;
        /**
         * @var int
         */
        private $ban_minutes;

        /**
         * @var int
         */
        private $ban_counts;
        /**
         * @var Translator
         */
        private $translator;
        /**
         * @var Request
         */
        private $request;
        /**
         * @var RequestStack
         */
        private $requestStack;

        use BanUsersTrait;

        /**
         * LoginType constructor.
         *
         * @param EntityManager $manager
         * @param ContainerInterface $container
         * @param RequestStack $requestStack
         * @param UserPasswordEncoder $encoder
         * @param Translator $translator
         */
        public function __construct(
            EntityManager $manager,
            ContainerInterface $container,
            RequestStack $requestStack,
            UserPasswordEncoder $encoder,
            Translator $translator
        ) {
            $this->manager      = $manager;
            $this->container    = $container;
            $this->request      = $requestStack->getCurrentRequest();
            $this->encoder      = $encoder;
            $this->ban_minutes  = $this->container->getParameter('ban_minutes');
            $this->ban_counts   = $this->container->getParameter('ban_max_count');
            $this->translator   = $translator;
            $this->requestStack = $requestStack;
        }


        /**
         * {@inheritdoc}
         */
        public function buildForm(FormBuilderInterface $builder, array $options) {
            $builder->add('email')
                    ->add('password');
            $builder->addEventListener(
                FormEvents::POST_SUBMIT,
                function (FormEvent $event) use ($options) {
                    if ($event->getForm()->getErrors(true, true)->count() > 0) {
                        return;
                    }
                    $user = $event->getData();
                    $ban  = $this->checkBanUser($event);
                    if ($event->getForm()->getErrors()->count() > 0) {
                        return;
                    }
                    $loadedUser = $this->manager->getRepository($options['data_class'])
                                                ->findOneBy(['email' => $user->getEmail()]);
                    if ( ! $loadedUser) {
                        $this->manageBanUsers($ban);
                        $event->getForm()->addError(
                            new FormError(
                                $this->translator->trans(
                                    "login.email or password is incorrect",
                                    [],
                                    "validators")));

                        return;
                    }
                    if ( ! $this->encoder->isPasswordValid(
                        $loadedUser,
                        $user->getPassword())) {
                        $this->manageBanUsers($ban);
                        $event->getForm()->addError(
                            new FormError(
                                $this->translator->trans(
                                    "login.email or password is incorrect",
                                    [],
                                    "validators")));

                        return;
                    }
                    $event->setData($loadedUser);
                });
        }

        /**
         * {@inheritdoc}
         */
        public function configureOptions(OptionsResolver $resolver) {
            $resolver->setDefaults(["csrf_protection" => false]);
        }

        /**
         * {@inheritdoc}
         */
        public function getBlockPrefix() {
            return 'appbundle_login';
        }


    }