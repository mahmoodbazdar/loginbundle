# Symfony 3 login bundle
## Installation
Add the repository to your `composer.json` file
```
"repositories": [
    {
        "type": "vcs",
        "url": "git@github.com:Bugloos/LoginBundle.git"
    }
]
```

and then require the project:
``
composer require Bugloos/LoginBundle
``

Composer will ask about your Github token.
## Dependency
This bundle has dependency to lexik/jwt-authentication-bundle Bundle, you have to config this module too.
You can find out how to config your bundle https://knpuniversity.com/screencast/symfony-rest4/lexikjwt-authentication-bundle
### Add to project

Add LoginBundle bundle to AppKernel.php
```php
$bundles = [
    "...",
    new \Bugloos\LoginBundle\LoginBundle(),    
    new \Lexik\Bundle\JWTAuthenticationBundle\LexikJWTAuthenticationBundle(),
];
```
And [enable](https://symfony.com/doc/current/translation.html#translation-configuration) Translation. 

You need to add this configuration to your `config.yml` file:
consider that these are default values, change the parameters based on your project.
```yaml
login:
    ban_minutes: 15
    ban_max_count: 10
```
We have a ban table in our database which prevents too much request for reset password or getting reset password link.

And before using the project you need to update your schema:

``
./bin/console doctrine:schema:update --force
``
## How to use it

Create a Class in AppBundle Namespace and implement class of our interface, in this way you are able to add your own properties.

```php
<?php

namespace AppBundle\Entity;

use Bugloos\LoginBundle\Entity\BugloosUserInterface;
    use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User implements BugloosUserInterface, UserInterface
{
    // ...       
}

```

It's important to implement `BugloosUserInterface` in your user entity. 
```php
interface BugloosUserInterface
{
    public function setEmail($email);

    public function getEmail();

    public function setPassword($password);

    public function getPassword();

    public function setToken($token);

    public function getToken();

    public function getLastRequest();

    public function setLastRequest($lastRequest);
}
```
## Final step
You are ready to user our service for following actions:
```php             
// Login
// If it is true it will return the token.
public function loginAction(Request $request)
{
  $token = $this->get('bugloos.login')->login($request);
  if ($token instanceof FormInterface) {
    throw new BadCredentialsException();
  }
  // Login
  // If it is true it will return the token.
}

//Reste password
// If it is true it will return user object otherwise it will be false
public function resetPasswordAction(Request $request)
{
  $user = $this->get('bugloos.login')->resetPassword($request);
}

// Forgot password
// If it is true the result will be an array=> hash for hash string and user is the user object
public function forgotPasswordAction(Request $request)
{
  $result = $this->get('bugloos.login')->forgotPassword($request);
  if ($result instanceof FormInterface) {
    throw new BadCredentialsException();
  }
  $hash = $result['hash'];
  $user = $result['user'];
}
```
