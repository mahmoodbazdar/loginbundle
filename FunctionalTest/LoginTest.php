<?php

    use GuzzleHttp\Exception\RequestException;
    use GuzzleHttp\Psr7;

    class LoginTest extends \PHPUnit\Framework\TestCase {

        public function testLogin() {
            try {
                $client   = new \GuzzleHttp\Client();
                $response = $client->post('http://localhost:8000/login',
                    [
                        'auth' => ['knightvoly@gmail.com', '12321'],
                    ]);
                $this->assertEquals(200, $response->getStatusCode());
            } catch (RequestException $e) {
                echo Psr7\str($e->getRequest());
                if ($e->hasResponse()) {
                    echo Psr7\str($e->getResponse());
                }
            }
        }

        public function testFail() {
            $client   = new \GuzzleHttp\Client();
            $response = $client->post('http://localhost:8000/login',
                [
                    'http_errors' => false,
                    'auth'        => ['knightvoly@gmail.com', '123212'],
                ]);
            $this->assertEquals(401, $response->getStatusCode());
        }

        public function testForgotPassword() {
            $client   = new \GuzzleHttp\Client();
            $response = $client->post('http://localhost:8000/forgot',
                [
                    'form_params' => [
                        'email' => 'knightvoly@gmail.com',
                    ],
                ]);
            $this->assertEquals(200, $response->getStatusCode());
        }
    }