<?php

    namespace Bugloos\LoginBundle\Exceptions;

    use Symfony\Component\HttpFoundation\Response;

    class ApiProblem
    {
        /**
         * @var integer $statusCode
         */
        private $statusCode;

        /**
         * @var string $type
         */
        private $type;

        /**
         * @var string $title
         */
        private $title;
        /**
         * @var array
         */
        private $extraData = array();

        public function __construct($statusCode, $type = null)
        {
            $this->statusCode = $statusCode;
            if ($type === null) {
                $type = 'about:blank';
                $title = isset(Response::$statusTexts[$statusCode])
                    ? Response::$statusTexts[$statusCode]
                    : 'Unknown status code :(';
            } else {
                $title = $type;
            }
            $this->type = $type;
            $this->title = $title;
        }

        public function toArray()
        {
            return array_merge(
                $this->extraData,
                array(
                    'status' => $this->statusCode,
                    'type'   => $this->type,
                    'title'  => $this->title,
                )
            );
        }

        /**
         * @return int
         */
        public function getStatusCode()
        {
            return $this->statusCode;
        }

        /**
         * @param int $statusCode
         */
        public function setStatusCode($statusCode)
        {
            $this->statusCode = $statusCode;
        }

        /**
         * @return string
         */
        public function getType()
        {
            return $this->type;
        }

        /**
         * @param string $type
         */
        public function setType($type)
        {
            $this->type = $type;
        }

        /**
         * @return string
         */
        public function getTitle()
        {
            return $this->title;
        }

        /**
         * @param string $title
         */
        public function setTitle($title)
        {
            $this->title = $title;
        }


        public function set($name, $value)
        {
            $this->extraData[$name] = $value;
        }
    }