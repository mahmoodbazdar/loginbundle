<?php

    namespace Bugloos\LoginBundle\Exceptions;

    use Symfony\Component\EventDispatcher\EventSubscriberInterface;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\HttpFoundation\RequestStack;
    use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
    use Symfony\Component\HttpKernel\KernelEvents;
    use Symfony\Component\HttpFoundation\JsonResponse;
    use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

    class ApiExceptionSubscriber implements EventSubscriberInterface
    {
        private $debug;
        /**
         * @var Request $request
         */
        private $request;

        /**
         * ApiExceptionSubscriber constructor.
         * @param $debug
         * @param RequestStack $requestStack
         */
        public function __construct($debug, RequestStack $requestStack)
        {
            $this->debug = $debug;
            $this->request = $requestStack->getCurrentRequest();
        }

        public function onKernelException(GetResponseForExceptionEvent $event)
        {
            $e = $event->getException();
            $statusCode = $e instanceof HttpExceptionInterface ? $e->getStatusCode() : 500;
            // allow 500 errors to be thrown
            if ($this->debug && $statusCode >= 500) {
                return $event->getResponse();
            }
            if ($e instanceof ApiProblemException) {
                $apiProblem = $e->getApiProblem();
            } else {


                $apiProblem = new ApiProblem(
                    $statusCode
                );
                if ($e instanceof HttpExceptionInterface) {
                    $apiProblem->set('detail', $e->getMessage());
                }
            }
            $data = $apiProblem->toArray();
            $response = new JsonResponse(
                $data,
                $apiProblem->getStatusCode()
            );
            $response->headers->set('Content-Type', 'application/problem+json');
            $event->setResponse($response);
            return $response;
        }

        public static function getSubscribedEvents()
        {
            return array(
                KernelEvents::EXCEPTION => 'onKernelException'
            );
        }
    }
