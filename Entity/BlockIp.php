<?php

    namespace Bugloos\LoginBundle\Entity;

    use Doctrine\ORM\Mapping as ORM;
    use Doctrine\ORM\Mapping\UniqueConstraint;

    /**
     * @ORM\Entity
     * @ORM\HasLifecycleCallbacks()
     * @ORM\Table(name="block_ip",uniqueConstraints={@UniqueConstraint(name="unique_ip", columns={"ip", "type"})})
     */
    class BlockIp
    {
        const LOGIN_TYPE = 'login';
        const FORGOT_PASSWORD_TYPE = 'forgot_password';
        /**
         * @var int
         *
         * @ORM\Column(name="id", type="integer")
         * @ORM\Id
         * @ORM\GeneratedValue(strategy="AUTO")
         */
        private $id;

        /**
         * @var string
         *
         * @ORM\Column(name="ip", type="string", length=255)
         */
        private $ip;

        /**
         * @var int
         *
         * @ORM\Column(name="counter", type="integer")
         */
        private $counter;

        /**
         * @var string
         *
         * @ORM\Column(name="type", type="string", length=255)
         */
        private $type;


        /**
         * @var \DateTime
         *
         * @ORM\Column(name="createdAt", type="datetime")
         */
        private $createdAt;

        /**
         * @var \DateTime
         *
         * @ORM\Column(name="UpdatedAt", type="datetime")
         */
        private $updatedAt;

        /**
         * Get id
         *
         * @return int
         */
        public function getId()
        {
            return $this->id;
        }

        /**
         * Set ip
         *
         * @param string $ip
         *
         * @return BlockIp
         */
        public function setIp($ip)
        {
            $this->ip = $ip;
            return $this;
        }

        /**
         * Get ip
         *
         * @return string
         */
        public function getIp()
        {
            return $this->ip;
        }

        /**
         * Set counter
         *
         * @param integer $counter
         *
         * @return BlockIp
         */
        public function setCounter($counter)
        {
            $this->counter = $counter;
            return $this;
        }

        /**
         * Get counter
         *
         * @return int
         */
        public function getCounter()
        {
            return $this->counter;
        }

        /**
         * Set type
         *
         * @param string $type
         *
         * @return BlockIp
         */
        public function setType($type)
        {
            $this->type = $type;
            return $this;
        }

        /**
         * Get type
         *
         * @return string
         */
        public function getType()
        {
            return $this->type;
        }

        /**
         * Set createdAt
         *
         * @param \DateTime $createdAt
         *
         * @return BlockIp
         */
        public function setCreatedAt($createdAt)
        {
            $this->createdAt = $createdAt;
            return $this;
        }

        /**
         * Get createdAt
         *
         * @return \DateTime
         */
        public function getCreatedAt()
        {
            return $this->createdAt;
        }

        /**
         * Set updatedAt
         *
         * @param \DateTime $updatedAt
         *
         * @return BlockIp
         */
        public function setUpdatedAt($updatedAt)
        {
            $this->updatedAt = $updatedAt;
            return $this;
        }

        /**
         * Get updatedAt
         *
         * @return \DateTime
         */
        public function getUpdatedAt()
        {
            return $this->updatedAt;
        }

        /**
         * @ORM\PrePersist()
         * @ORM\PreUpdate()
         */
        public function updatedTimestamps()
        {
            $this->setUpdatedAt(new \DateTime('now'));
            if ($this->getCreatedAt() == null) {
                $this->setCreatedAt(new \DateTime('now'));
            }
        }
    }

