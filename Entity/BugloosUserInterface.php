<?php

    namespace Bugloos\LoginBundle\Entity;

    interface BugloosUserInterface
    {

        public function setEmail($email);

        public function getEmail();

        public function setPassword($password);

        public function getPassword();

        public function setToken($token);

        public function getToken();

        public function getLastRequest();

        public function setLastRequest($lastRequest);

    }